package apka2.hello;

public class Greeting {

   public final long id;
    private final String content;
    public final String type;

    public Greeting(long id, String content) {
        this.id = id;
        this.content = content;
        this.type = "przyklad";
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public String getType(){
        return type;
    }
}
