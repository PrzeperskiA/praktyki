package Hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ListController {
    @RequestMapping("/list")
    @ResponseBody
    class Lista {
        Hello.ListMain list = new Hello.ListMain();
    }
}