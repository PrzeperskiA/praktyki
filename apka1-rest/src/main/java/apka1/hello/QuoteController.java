package apka1.hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QuoteController {
    private static final String template;

    static {
        template = "Hi, %s!";
    }

    @RequestMapping("/wynikoncowy")
    public Quote quote(@RequestParam(value = "name", defaultValue = "User") String name) {
        Quote quote = new Quote();
        quote.setType(String.format(template, name));
        return quote;
    }
}
