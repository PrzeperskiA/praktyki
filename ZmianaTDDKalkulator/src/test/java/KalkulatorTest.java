import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import tdd.Dodawanie;

import static org.assertj.core.api.BDDAssertions.then;

class DodawanieTests {

    @DisplayName("Should add argument from array")
    @Test
    void shouldAddArgumentsFromArray() {
        //given
        Dodawanie dodawanie = new Dodawanie();
        //when
        int result = dodawanie.dodaj(new int[]{2, 3, 4});
        //then
        then(result).isEqualTo(9);
    }
}